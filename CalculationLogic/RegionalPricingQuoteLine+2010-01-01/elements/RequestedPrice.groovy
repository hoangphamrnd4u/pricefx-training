def entryName = 'RequestedPrice'
def label = 'Requested Price'
api.decimalUserEntry(entryName)
api.getParameter(entryName)
        .setLabel(label)
        .setConfigParameter('inputType', 'range')
        .setConfigParameter('from', 0)