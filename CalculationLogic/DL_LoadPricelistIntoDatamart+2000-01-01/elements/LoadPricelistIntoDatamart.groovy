//  Iterate through all PL that has property = value:
//      Create a Map:
//          For each PL => add to map [ PL.id : PL.targetDate ]
def property = "label"
def value = "PL_LabR2"
def filters = [
        Filter.equal(property, value)
]
def pricelistIterator = api.stream("PL", null, *filters)
def targetDates = pricelistIterator?.collectEntries { pl -> [(pl.id): (pl.targetDate)] }
pricelistIterator?.close()

// TODO place your code here
def target = api.getDatamartRowSet("target")
def newRow
def targetDate;
//  Iterates through all products in every PLs
//      If the product is in one of the pricelistIterator from above, then add to newRow
//      newRow will then be loaded to target
def plItemsIterator = api.stream("PLI", "sku")
plItemsIterator.each { product ->
    targetDate = targetDates[product.pricelistId]
    if (targetDate) {
        newRow = [
                ProductId : product.sku,
                TargetDate: targetDate,
                ResultPrice: product.resultPrice,
                Currency: product.currency
        ]
        targetDate = null
        target?.addRow(newRow)
    }
}
plItemsIterator?.close()

