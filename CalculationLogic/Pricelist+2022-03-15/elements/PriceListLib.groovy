BigDecimal getMarginAdj(){
    return api.vLookup("MarginAdj", api.product("ProductGroup"))
}

BigDecimal getAttributeAdj(){
    return api.vLookup("AttributeAdj", "AttributeAdj", api.product("ProductLifeCycle"))
}