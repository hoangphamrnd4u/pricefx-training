def country = api.customer("Country", input.Customer)

def keys = [
        Country: country,
        DeliveryType: input.DeliveryType
]

return api.vLookup("FreightSurcharge", "FreightSurcharge", keys)

/*
    vLookup(string1, string2, map)
    string1: name of the table we want to search in (this table is one of price param tables)
    string2: name or label of the column inside that table => return the value inside that column
    map: search criteria.

    In this case: "Select FreightSurcharge in FreightSurcharge
                        where Country = ? and DeliveryType = ?"
*/
