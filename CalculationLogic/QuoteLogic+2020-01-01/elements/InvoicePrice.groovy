def salesDiscountAbs = out.SalesDiscountAbs
def listPrice = out.ListPrice
if (salesDiscountAbs == null || listPrice == null){
    api.addWarning("Cannot calculate Invoice Price: Missing parameter(s)")
    return null
}
return listPrice - salesDiscountAbs