def invoicePrice = out.InvoicePrice
def cost = out.Cost
if (invoicePrice == null || cost == null){
    api.addWarning("Cannot calculate Pocket Margin Money: Missing parameter(s)")
    return null
}
return invoicePrice - cost