def listPrice = out.ListPrice
def quantity = input.Quantity
def freightSurcharge = out.FreightSurcharge
if (listPrice == null || quantity == null || freightSurcharge == null){
    api.addWarning("Cannot calculate Total Invoice Price: Missing parameter(s)")
    return null
}
return listPrice * quantity + freightSurcharge