def pocketMarginAbs = out.PocketMarginAbs
def listPrice = out.ListPrice

if (pocketMarginAbs == null || listPrice == null) {
    api.addWarning("Cannot calculate Pocket Margin %: Misisng parameter(s)")
    return null
}
if (listPrice == 0) {
    api.addWarning("Cannot calculate Pocket Margin %: List Price is 0 => Division by 0")
    return null
}

def pocketMarginPct = pocketMarginAbs / listPrice
//if (pocketMarginPct < 0){
//    api.redAlert('Pocket Margin % is too low')
//}

//  Get alert thresholds
String productGroup = api.product("ProductGroup")
api.trace("productGroup", null, productGroup)
String thresholdTableName = "PocketMarginAlertThreshold"
def yellowThreshold = api.vLookup(thresholdTableName, "Threshold", productGroup, "Yellow")
def redThreshold = api.vLookup(thresholdTableName, "Threshold", productGroup, "Red")
def criticalThreshold = api.vLookup(thresholdTableName, "Threshold", productGroup, "Critical")
api.trace("yellowThreshold", null, yellowThreshold)
api.trace("redThreshold", null, redThreshold)
api.trace("criticalThreshold", null, criticalThreshold)


//  Check thresholds
if (criticalThreshold && pocketMarginPct < criticalThreshold) {
    api.criticalAlert("Pocket Margin % is unacceptable low")
} else if (redThreshold && pocketMarginPct < redThreshold) {
    api.redAlert("Pocket Margin % is very low")
} else if (yellowThreshold && pocketMarginPct < yellowThreshold) {
    api.yellowAlert("Pocket Margin % is low")
}

return pocketMarginPct