def listPrice = out.ListPrice
def salesDiscountPct = out.SalesDiscountPct
if (listPrice == null || salesDiscountPct == null) {
    api.addWarning("Cannot calculate Sales Discount: Missing parameter(s)")
    return null
}
return listPrice * salesDiscountPct