def start = 0
def data = null
def fields = ["sku", "attribute1", "attribute2"]
int maxFindResultsLimit = api.getMaxFindResultsLimit()

while (data = api.find("P", start, maxFindResultsLimit, "sku", fields)) {
    start += data.size()
    for (row in data) {
        def out = [
                sku: row.sku,
                ProductGroup: row.attribute1,
                BusinessUnit: row.attribute2
        ]
        api.trace("row", out)
    }
}