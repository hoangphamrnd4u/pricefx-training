def fields = ["sku", "attribute1", "attribute2"]

def iterator = api.stream("P", "sku", fields)
while (iterator?.hasNext()) {
    def comp = iterator.next()
    if (comp.sku != null && comp.attribute1 != null) {
        def row = [
                sku: comp.sku,
                ProductGroup: comp.attribute1,
                BusinessUnit: comp.attribute2
        ]
        api.trace("Row", row)
    }
}
iterator?.close()