import java.math.RoundingMode

BigDecimal round(BigDecimal input, int decimalPlace) {
    if (!input) {
        return null
    }
    return input.setScale(decimalPlace, RoundingMode.HALF_UP)
}