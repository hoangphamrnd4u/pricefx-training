def chart = api.newChartBuilder()
    .newWaterfall()
      .getOptions()
		.setHideTooltips(false)
		.back()
      .getSeries()
		.setLabel('Waterfall Data')
		.setDatamart('DM.Transaction')
		.setCurrency('EUR')
		.getAggregation().withTotal().back()
		.setHideDataLabels(false)
		.addDimFilter('InvoiceDateYear', out.Year as String)
		.addWaterfallColumn(WaterfallColumnType.RESULT, 'ListPrice', 'undefined')
		.addWaterfallColumn(WaterfallColumnType.SUBTRACT, 'Discount', 'undefined')
		.addWaterfallColumn(WaterfallColumnType.RESULT, 'InvoicePrice', 'undefined')
		.addWaterfallColumn(WaterfallColumnType.SUBTRACT, 'Cost', 'undefined')
		.addWaterfallColumn(WaterfallColumnType.RESULT, 'Margin', 'undefined')
		.setGeneratedQueryDto('{"name":null,"datamart":"DM.Transaction","label":"Waterfall Data","limit":null,"source":"DM.Transaction","projections":{"pp_ListPrice":{"advancedProjection":true,"expression":"SUM({field})","function":null,"parameters":{"field":"ListPrice","quantity":"Quantity","base":"ListPrice"},"default":0,"formatString":"∑{field}","label":"ListPrice","name":"ListPrice","alias":"pp_ListPrice"},"na_Discount":{"advancedProjection":true,"expression":"SUM({field})","function":null,"parameters":{"field":"Discount","quantity":"Quantity","base":"ListPrice"},"default":0,"formatString":"∑{field}","label":"Discount","name":"Discount","alias":"na_Discount"},"pp_InvoicePrice":{"advancedProjection":true,"expression":"SUM({field})","function":null,"parameters":{"field":"InvoicePrice","quantity":"Quantity","base":"ListPrice"},"default":0,"formatString":"∑{field}","label":"InvoicePrice","name":"InvoicePrice","alias":"pp_InvoicePrice"},"na_Cost":{"advancedProjection":true,"expression":"SUM({field})","function":null,"parameters":{"field":"Cost","quantity":"Quantity","base":"ListPrice"},"default":0,"formatString":"∑{field}","label":"Cost","name":"Cost","alias":"na_Cost"},"pp_Margin":{"advancedProjection":true,"expression":"SUM({field})","function":null,"parameters":{"field":"Margin","quantity":"Quantity","base":"ListPrice"},"default":0,"formatString":"∑{field}","label":"Margin","name":"Margin","alias":"pp_Margin"}},"options":{"currency":"EUR"},"filter":{},"dimensionFilters":[],"rollup":true,"sortBy":[]}')
		.back()
      
      .getDictionary()
        .buildFromOpaqueString('[{"sectionIdx":1,"category":"PROJECTION","categoryLabel":"Projection","defaultValue":null,"key":"x","keyLabel":"Axis X","sectionLabel":"Waterfall Data"},{"sectionIdx":1,"category":"PROJECTION","categoryLabel":"Projection","defaultValue":null,"key":"y","keyLabel":"Axis Y","sectionLabel":"Waterfall Data"}]')
        .back()
      .build()
          chart.controllerOptions.editGenericFilter=false
          chart.controllerOptions.editDimFilters=false
          chart.controllerOptions.defineDimFilters=false
          return chart