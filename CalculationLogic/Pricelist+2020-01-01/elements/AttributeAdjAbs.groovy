def attributeAdjPct = out.AttributeAdjPct
def basePrice = out.BasePrice
def marginAdjAbs = out.MarginAdjAbs

if (attributeAdjPct == 1) {
    api.addWarning("Attribute Adjustment % cannot be 100% => division by 0")
    return null
}

return (basePrice + marginAdjAbs) * (attributeAdjPct / (1 - attributeAdjPct))