def basePrice = out.BasePrice
def marginAdjAbs = out.MarginAdjAbs
def attributeAdjAbs = out.AttributeAdjAbs
def sku = api.product("sku")

if (basePrice == null || marginAdjAbs == null || attributeAdjAbs == null) {
    api.addWarning("Cannot calculate List Price for item: ${sku} because of missing parameter(s)")
}

return basePrice + marginAdjAbs + attributeAdjAbs