def avgCost = api.productExtension("ProductCost")?.find()?.attribute1
api.trace("avgCost", null, avgCost)
api.trace("productSKU", null, api.product("sku"))
if (avgCost == null) {
    api.addWarning("Could not find Average Cost in PX table ProductCost")
}
return avgCost