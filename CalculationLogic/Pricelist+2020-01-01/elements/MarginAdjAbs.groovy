def basePrice = out.BasePrice
def marginAdjPct = out.MarginAdjPct

if (basePrice == null || marginAdjPct == null) {
    api.addWarning("Margin Adjustment cannot be calculated: missing parameter(s)")
    return null
}

if (marginAdjPct == 1) {
    api.addWarning("Margin Adjustment % cannot be 100% => division by zero")
    return null
}

return basePrice * (marginAdjPct / (1 - marginAdjPct))