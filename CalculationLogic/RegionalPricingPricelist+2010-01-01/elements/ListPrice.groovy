def basePrice = out.BasePrice
def marginAdj = out.MarginAdj
def attributeAdj = out.AttributeAdj
def packagingAdj = out.PackagingAdj
def taxAdj = out.TaxTariffAdj
if (basePrice == null || marginAdj == null || attributeAdj == null || packagingAdj == null || taxAdj == null) {
    api.addWarning('Cannot calculate List Price: Missing parameter(s)')
    return null
}
return basePrice + marginAdj + attributeAdj + packagingAdj + taxAdj