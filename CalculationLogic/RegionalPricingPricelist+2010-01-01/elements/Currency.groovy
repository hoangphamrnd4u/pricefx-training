def regionInputs = input.Region // returns a list containing 1 string
if (regionInputs == null){
    api.addWarning('Cannot get currency: Missing region input')
    return null
}
def region = regionInputs.find() // get the first item inside the list
return api.vLookup('Region','Currency', region as String)