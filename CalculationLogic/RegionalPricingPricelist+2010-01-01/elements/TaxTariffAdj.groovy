def taxAdjPct = api.vLookup('TaxAdj', input.Region?.find())
def basePrice = out.BasePrice
if (basePrice == null || taxAdjPct == null){
    api.addWarning('Cannot calculate Tax Adjustment: Missing parameter(s)')
    return null
}
return basePrice * taxAdjPct