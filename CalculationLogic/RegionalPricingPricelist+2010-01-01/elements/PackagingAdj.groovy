def packagingAdjPct = api.vLookup('PackagingAdj', 'PackagingAdj', api.product('Size'))
def basePrice = out.BasePrice
if (basePrice == null || packagingAdjPct == null){
    api.addWarning('Cannot calculate Packaging Adjustment: Missing parameter(s)')
    return null
}
return basePrice * packagingAdjPct
