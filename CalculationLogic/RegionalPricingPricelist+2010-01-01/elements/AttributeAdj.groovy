def attributeAdjPct = api.vLookup('AttributeAdj', 'AttributeAdj', api.product('ProductLifeCycle'))
def basePrice = out.BasePrice
if (attributeAdjPct == null || basePrice == null){
    api.addWarning('Cannot calculate Attribute Adjustment: Missing parameter(s)')
    return null
}
return basePrice * attributeAdjPct