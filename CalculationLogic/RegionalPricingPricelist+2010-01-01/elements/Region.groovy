def custRegionEU = "Europe"
def custRegionUS = "America"

def entryName = "Region"
def options = [custRegionEU, custRegionUS]
def labels = [
        Europe : custRegionEU,
        America: custRegionUS
]

return api.inputBuilderFactory()
        .createOptionsEntry(entryName)
        .setLabel(entryName)
        .setRequired(true)
        .setOptions(options)
        .setLabels(labels)
        .getInput()
