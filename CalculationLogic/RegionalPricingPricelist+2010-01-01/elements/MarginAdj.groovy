def marginAdjPct = api.vLookup('MarginAdj', api.product('ProductGroup'))
def basePrice = out.BasePrice
if (marginAdjPct == null || basePrice == null) {
    api.addWarning('Cannot calculate Margin Adjustment: Missing parameter(s)')
    return null
}
return basePrice * marginAdjPct