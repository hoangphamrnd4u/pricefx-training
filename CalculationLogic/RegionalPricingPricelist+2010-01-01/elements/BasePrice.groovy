def basePrice = api.productExtension("ProductCost")?.find()?.attribute1
def currency = out.Currency
if (currency == null || basePrice == null){
    api.addWarning('Cannot calculate Base Price: Missing parameter(s)')
    return null
}
if (currency != 'EUR'){
    basePrice = libs.MoneyUtils.ExchangeRate.convertMoney(basePrice, 'EUR', 'USD')
}
return basePrice