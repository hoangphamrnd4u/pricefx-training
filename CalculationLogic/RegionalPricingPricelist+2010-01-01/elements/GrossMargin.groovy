def basePrice = out.BasePrice
def listPrice = out.ListPrice
if (basePrice == null || listPrice == null) {
    api.addWarning('Cannot calculate Gross Margin: Missing parameter(s)')
    return null
}
def grossMargin = (listPrice - basePrice) / listPrice
def businessUnit = api.product('BusinessUnit') as String

//  TODO Investigate why api.vLookup('PriceStrategy', attributes, keys) is not working???
//def attributes = ['attribute1', 'attribute2', 'attribute3']
//def keys = [
//        name: businessUnit
//]
//def marginThresholds = api.vLookup('PriceStrategy', attributes, keys)
//api.trace('thresholds', marginThresholds)
//def yellowThreshold = marginThresholds?.attribute3
//def redThreshold = marginThresholds?.attribute2
//def criticalThreshold = marginThresholds?.attribute1
//

def yellowThreshold = api.vLookup('PriceStrategy', 'attribute3', businessUnit)
def redThreshold = api.vLookup('PriceStrategy', 'attribute2', businessUnit)
def criticalThreshold = api.vLookup('PriceStrategy', 'attribute1', businessUnit)
if (yellowThreshold != null && redThreshold != null && criticalThreshold != null) {
    def sku = api.product('sku')
    if (grossMargin <= criticalThreshold) {
        api.criticalAlert("Gross Margin is extremely low for item: ${sku}")
    } else if (grossMargin <= redThreshold) {
        api.redAlert("Gross Margin is very low for item: ${sku}")
    } else if (grossMargin <= yellowThreshold) {
        api.yellowAlert("Gross Margin is low for item: ${sku}")
    }
}
return grossMargin