import net.pricefx.common.api.CalculationResultType
import net.pricefx.common.api.FieldFormatType

if (quoteProcessor.isPostPhase()) {
    def sumMargin = out.SumTotalMargin
    def sumInvoicePrice = out.SumTotalInvoicePrice
    def warnings = null
    def totalMarginPct = 0.0
    if (!libs.MySharedLib.CheckNullUtils.validateNull(sumMargin, sumInvoicePrice)) {
        warnings = ["Cannot calculate Sum Margin %: Missing Parameter(s)"]
    }
    totalMarginPct = sumMargin / sumInvoicePrice

    def output = [
            resultName   : "TotalMarginPct",
            resultLabel  : "Total Margin %",
            result       : totalMarginPct,
            formatType   : FieldFormatType.PERCENT,
            resultType   : CalculationResultType.SIMPLE,
            cssProperties: "background-color:#99FFDD",
            warnings     : warnings
    ]
    def a = quoteProcessor.addOrUpdateOutput(output)
    api.trace("a", a)
    return totalMarginPct
}