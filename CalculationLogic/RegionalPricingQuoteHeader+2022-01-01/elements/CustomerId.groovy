if (quoteProcessor.isPrePhase()){
    def paramName = 'Customer'
    def label = 'Customer'

    def customerId = api.inputBuilderFactory()
    .createCustomerEntry(paramName)
    .setLabel(label)
    .setRequired(true)
    .buildMap()

    return quoteProcessor.addOrUpdateInput(customerId)
}