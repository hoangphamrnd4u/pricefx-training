import net.pricefx.common.api.CalculationResultType
import net.pricefx.common.api.FieldFormatType

if (quoteProcessor.isPostPhase()) {
    def sum = 0.0
    def currentItemTotalInvoicePrice = 0.0
    def lineItems = out.LineItems as List<Object>
    def warnings = null
    lineItems.each { item ->
        currentItemTotalInvoicePrice = item.outputs?.find { output ->
            output.resultName == 'TotalInvoicePrice'
        }?.result as BigDecimal

        if (!libs.MySharedLib.CheckNullUtils.validateNull(currentItemTotalInvoicePrice)) {
            warnings = ["Cannot calculate Sum of Total Invoice Price: Missing Total Invoice Price of item: ${item.sku}"]
        }

        sum += currentItemTotalInvoicePrice
    }

    def output = [
            resultName   : "SumTotalInvoicePrice",
            resultLabel  : "Sum of Total Invoice Price",
            result       : sum,
            formatType   : FieldFormatType.MONEY,
            resultType   : CalculationResultType.SIMPLE,
            cssProperties: "background-color:#99FFDD",
            warnings     : warnings
    ]
    quoteProcessor.addOrUpdateOutput(output)

    return libs.MySharedLib.RoundingUtils.round(sum, 2)
}