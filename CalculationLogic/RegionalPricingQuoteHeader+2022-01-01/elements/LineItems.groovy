return quoteProcessor.quoteView.lineItems.findAll {
    // Only add item to lineItems if the item isn't a folder (folder is a boolean property of each item in the Quote)
    !it.folder
}