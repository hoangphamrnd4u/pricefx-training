import net.pricefx.common.api.CalculationResultType
import net.pricefx.common.api.FieldFormatType

if (quoteProcessor.isPostPhase()) {
    def lineItems = out.LineItems as List<Object>
    def sum = 0.0
    def currentItemTotalMargin = 0.0
    lineItems.each { item ->
        currentItemTotalMargin = item.outputs?.find { output ->
            output.resultName == 'TotalMargin'
        }?.result as BigDecimal
        if (!libs.MySharedLib.CheckNullUtils.validateNull(currentItemTotalMargin)) {
            api.addWarning("Cannot calculate Sum of Margin: Missing parameter(s) for item: ${item.sku}")

        }
        sum += currentItemTotalMargin
    }

    return sum
}