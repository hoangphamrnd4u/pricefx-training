def dmCtx = api.getDatamartContext()
def salesDM = dmCtx.getDatamart("Transaction")
def filters = [
        Filter.equal("CustomerId", out.Customer)
]

def dmQuery = dmCtx.newQuery(salesDM) //default rollUp = true
        .select("CustomerId", "Customer ID")
        .select("COUNTDISTINCT(InvoiceDate)", "invoiceCount")
        .select("SUM(Margin)/COUNTDISTINCT(InvoiceDate)", "Avg Profit")
        .select('SUM(InvoicePrice)', 'Total Revenue')
        .select('SUM(Margin)', 'Total Profit')
        .select('SUM(Quantity)', 'Total Qty')
        .select('SUM(Discount)', 'Total Discount')
        .where(*filters)

def result = dmCtx.executeQuery(dmQuery)
result?.each {row ->
    def i = 0
    api.trace('query', i as String, row.data.getRowValues(i++))
}