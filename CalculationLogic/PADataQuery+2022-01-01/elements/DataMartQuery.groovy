def dmCtx = api.getDatamartContext()
def salesDM = dmCtx.getDatamart("Transaction")
def filters = [
        Filter.equal("CustomerId", out.Customer)
]

def dmQuery = dmCtx.newQuery(salesDM, false)
        .select("CustomerId")
        .select("Cost")
        .select("InvoiceDate")
        .where(*filters)

def result = dmCtx.executeQuery(dmQuery)
result?.getData()?.each {row ->
    api.trace("Customer Id", row.CustomerId)
    api.trace("Cost", row.Cost)
    api.trace("InvoiceDate", row.InvoiceDate)
    api.trace("=========", null)
}
