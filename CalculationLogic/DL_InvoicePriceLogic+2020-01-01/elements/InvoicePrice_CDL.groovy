if(api.isSyntaxCheck()) {
    api.userEntry("ListPrice")
    api.userEntry("Discount")
} else {
    if(input.ListPrice == null || input.Discount == null) {
        return null
    }
    api.trace("list price", input.ListPrice)
    api.trace("discount", input.Discount)
    return input.ListPrice - input.Discount
}