if (quoteProcessor.isPrePhase()) {
    return
}

List<Object> lineItems = quoteProcessor.quoteView.lineItems.findAll {
    !it.folder
    // Only add item to lineItems if the item isn't a folder (folder is a boolean property of each item in the Quote)
}

def sum = 0.0
def warnings = null
for (lineItem in lineItems) {
    BigDecimal price = lineItem?.outputs?.find { lineItemOutput ->
        lineItemOutput.resultName == "TotalInvoicePrice"
    }?.result

    if (price == null) {
        sum = null
        warnings = ["Unable to calculate: Value for TotalInvoicePrice is missing on one of the line items."]
        break
    }

    sum += price
}

def output = [
        resultName   : "TotalInvoicePrice",
        resultLabel  : "Total Invoice Price",
        result       : sum,
        formatType   : "MONEY_EUR",
        resultType   : "SIMPLE",
        cssProperties: "background-color:#99FFDD",
        warnings     : warnings
]

return quoteProcessor.addOrUpdateOutput(output)
