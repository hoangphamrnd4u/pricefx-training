if (quoteProcessor.isPostPhase()) {
    return null
}

def deliveryType = api.inputBuilderFactory()
        .createOptionEntry("DeliveryType")
        .setLabel("Delivery Type")
        .setRequired(true)
        .setOptions(["Express", "Standard"])
        .setLabels(["Express": "Express Entry", "Standard": "Standard Delivery"])
        .buildMap()

return quoteProcessor.addOrUpdateInput(deliveryType)