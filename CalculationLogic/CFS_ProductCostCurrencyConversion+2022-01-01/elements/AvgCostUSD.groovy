def cost = api.currentItem().attribute1
//def exchangeRateKey = [
//        key1: "EUR",
//        key2: "USD",
//        key3: "2020-01-01"
//]
//def conversionRate = api.vLookup("ExchangeRate", "ExchangeRate", exchangeRateKey)
//api.trace("xchangeRate", conversionRate)

def date = api.targetDate().format("yyyy-MM-dd")

def filters = [
        Filter.equal("key1", "EUR"),  // From Ccy
        Filter.equal("key2", "USD"),    // To Ccy
        Filter.lessOrEqual("key3", date),           // Valid From
        Filter.greaterOrEqual("attribute1", date),  // Valid To
]

def exchangeRate = api.findLookupTableValues("ExchangeRate",
        *filters)?.find()?.attribute2

if (cost == null || exchangeRate == null) {
    api.addWarning("Cannot calculate Average Cost (USD): Missing Parameter(s)")
    return null
}

return cost * exchangeRate