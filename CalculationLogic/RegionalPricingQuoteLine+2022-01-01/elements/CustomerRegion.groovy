def customerId = out.CustomerId as String
if (customerId == null) {
    api.addWarning('Cannot retrieve Customer Region: Missing Customer Id')
    return null
}
return api.customer('Region', customerId)