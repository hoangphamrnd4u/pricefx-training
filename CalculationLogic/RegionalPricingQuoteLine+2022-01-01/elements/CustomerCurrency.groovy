def customerId = out.CustomerId as String
if (customerId == null){
    api.addWarning('Cannot retrieve Customer Currency: Missing Customer Id')
    return null
}
return api.customer('CustomerCurrency', customerId)