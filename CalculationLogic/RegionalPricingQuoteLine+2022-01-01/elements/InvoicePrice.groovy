def requestedInvoicePrice = input.RequestedPrice as BigDecimal
def listPrice = out.ListPrice as BigDecimal
def result
if (requestedInvoicePrice != null) {
    result = requestedInvoicePrice
} else if (listPrice == null) {
    api.addWarning('Cannot calculate Invoice Price: Missing List Price')
    return null
} else {
    result = listPrice
}
return libs.MySharedLib.RoundingUtils.round(result, 2)
