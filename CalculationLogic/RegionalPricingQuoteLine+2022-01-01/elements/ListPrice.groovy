def region = out.CustomerRegion as String
if (region == null) {
    api.addWarning('Cannot retrieve List Price: Missing Customer Region')
    return null
}

def plName
def regionEu = 'Europe'
def regionUs = 'America'
if (region != regionEu && region != regionUs) {
    api.addWarning('The logic only supports customers come from Europe or America')
    return null
} else {
    plName = region
}
def customerCurrency = out.CustomerCurrency as String
def currencyEur = 'EUR'
def currencyUsd = 'USD'
def regionCurrency = api.vLookup('Region', 'Currency', region) as String
if (customerCurrency != null && customerCurrency != currencyEur && customerCurrency != currencyUsd) {
    if (regionCurrency == null){
        api.addWarning('The logic only supports customers come from Europe or America')
        return null
    }
    return libs.MySharedLib.RoundingUtils.round(
            libs.MoneyUtils.ExchangeRate.convertMoney(api.pricelist(plName) as BigDecimal, regionCurrency, customerCurrency), 2
    )
}
return libs.MySharedLib.RoundingUtils.round(api.pricelist(plName) as BigDecimal, 2)