if (api.isSyntaxCheck()) {
    def entryName = 'Quantity'
    def label = 'Required Quantity'
    api.integerUserEntry(entryName)
    return api.getParameter(entryName)
            .setRequired(true)
            .setLabel(label)
            .setConfigParameter('inputType', 'range')
            .setConfigParameter('from', 1)

}
