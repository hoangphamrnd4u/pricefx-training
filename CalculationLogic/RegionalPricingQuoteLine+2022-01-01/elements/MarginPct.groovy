def margin = out.Margin as BigDecimal
def invoicePrice = out.InvoicePrice as BigDecimal
if (!libs.MySharedLib.CheckNullUtils.validateNull(margin, invoicePrice)) {
    api.addWarning('Cannot calculate Margin %: Missing parameter(s)')
    return null
}
return libs.MySharedLib.RoundingUtils.round((margin / invoicePrice), 2)