def listPrice = out.ListPrice as BigDecimal
def invoicePrice = out.InvoicePrice as BigDecimal
def salesDiscountPct
def businessUnit
def yellowThreshold
def redThreshold
def criticalThreshold
def customerRegion
if (listPrice == null || invoicePrice == null) {
    api.addWarning('Cannot calculate Sales Discount %: Missing parameter(s)')
    return null
}
salesDiscountPct = (listPrice - invoicePrice) / listPrice
businessUnit = out.BusinessUnit as String
if (businessUnit == null) {
    return salesDiscountPct
} else {
    customerRegion = out.CustomerRegion as String
    yellowThreshold = api.vLookup(
            'SalesDiscountThreshold', 'YellowAlert', customerRegion, businessUnit) as BigDecimal
    redThreshold = api.vLookup(
            'SalesDiscountThreshold', 'RedAlert', customerRegion, businessUnit) as BigDecimal
    criticalThreshold = api.vLookup(
            'SalesDiscountThreshold', 'CriticalAlert', customerRegion, businessUnit) as BigDecimal
    if (libs.MySharedLib.CheckNullUtils.validateNull(yellowThreshold, redThreshold, criticalThreshold)) {
        if (salesDiscountPct > criticalThreshold) {
            api.criticalAlert("The Sales Discount % is extremely high")
        } else if (salesDiscountPct > redThreshold) {
            api.redAlert("The Sales Discount % is very high")
        } else if (salesDiscountPct > yellowThreshold) {
            api.yellowAlert("The Sales Discount % is high")
        }
    }
}
return libs.MySharedLib.RoundingUtils.round(salesDiscountPct, 2)