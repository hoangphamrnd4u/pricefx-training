def invoicePrice = out.InvoicePrice
def avgCost = out.AvgCost
if (invoicePrice == null || avgCost == null) {
    api.addWarning('Cannot calculate Margin: Missing parameter(s)')
    return null
}
return invoicePrice - avgCost