def customerCurrency = out.CustomerCurrency as String
def pxCurrency = 'EUR'
def avgCost = api.productExtension('ProductCost',)?.find()?.attribute1 as BigDecimal
if (customerCurrency != pxCurrency) {
    avgCost = libs.MoneyUtils.ExchangeRate.convertMoney(avgCost, pxCurrency, customerCurrency)
}
return libs.MySharedLib.RoundingUtils.round(avgCost, 2)
