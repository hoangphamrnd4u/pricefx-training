def quantity = input.Quantity
def invoicePrice = out.InvoicePrice
if (!libs.MySharedLib.CheckNullUtils.validateNull(quantity, invoicePrice)) {
    api.addWarning('Cannot calculate Total Invoice Price: Missing parameter(s)')
    return null
}
return invoicePrice * quantity