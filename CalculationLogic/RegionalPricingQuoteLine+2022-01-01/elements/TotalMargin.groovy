def margin = out.Margin as BigDecimal
def quantity = input.Quantity as BigDecimal
if (!libs.MySharedLib.CheckNullUtils.validateNull(margin, quantity)) {
    api.addWarning('Cannot calculate Total Margin: Missing parameter(s)')
    return null
}
return libs.MySharedLib.RoundingUtils.round(margin * quantity, 2)