def listPrice = out.ListPrice
def avgCost = out.ProductCost
if (listPrice == null || avgCost == null) {
    api.addWarning("Cannot calculate Margin %: Missing parameter(s)")
    return null
}
return (listPrice - avgCost) / listPrice
