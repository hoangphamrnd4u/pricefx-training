def avgCost =  api.productExtension("ProductCost")?.find()?.attribute1
if (avgCost == null) {
    api.addWarning("Cannot find Cost of this product SKU")
}
return avgCost