/* This code sample is only for training purposes. Production code would use more filters. */
api.trace("target date", null, api.targetDate()?.format("yyyy-MM-dd"))
def competition = api.productCompetition(
        Filter.equal("competitionType", "Online"),
        Filter.lessThan("infoDate", api.targetDate()?.format("yyyy-MM-dd"))
)?.sort { a, b -> b.infoDate <=> a.infoDate }
        ?.find()
if (competition == null) {
    api.addWarning("Cannot find competition for this SKU")
    return null
}
return competition