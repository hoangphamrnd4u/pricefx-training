def competitionProductPrice = out.CompetitorProductPrice
def competitivePositioning = out.CompetitivePositioning
if (competitionProductPrice == null || competitivePositioning == null) {
    api.addWarning("Cannot calculate List Price: Missing parameter(s)")
    return null
}
return competitionProductPrice * competitivePositioning