def filters = [
        Filter.equal('pricelistId', pricelist.id)
]
def itemsIterator = api.stream('PLI', 'sku', *filters)
def businessUnit
def marginThreshold
def marginPct
def reasons = []
itemsIterator.each { item ->
    businessUnit = api.product("BusinessUnit", item.sku)
    marginThreshold = api.vLookup('PriceListApprovalLevels', 'MarginThreshold', businessUnit)
    marginPct = MyUtils.getMarginPct(item)
    if (marginPct == null) {
        reasons = ["Cannot calculate expected profitability: Missing parameter(s) [Margin %] for PLI: ${item.sku}"]
    }
    else if (marginThreshold > marginPct){
        reasons << "PLI ${item.sku} has low margin (${marginPct} < ${marginThreshold})"
    }
}
itemsIterator.close()

return reasons