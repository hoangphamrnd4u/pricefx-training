def reasons = out.CheckMarginThreshold

api.trace('reasons', reasons)

workflow.addWatcherStep('Price Manager')
        .withUserGroupWatchers('PriceManager')

if (reasons) {
    workflow.addApprovalStep('Product Manager')
            .withUserGroupApprovers('ProductManager')
            .withReasons(reasons.join('. '))
}