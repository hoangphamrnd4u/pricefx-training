//  Find total invoice price of the quote
//      Check if there's total invoice price in quote header
def totalInvoicePrice = quote.outputs.find {
    it.resultName == "TotalInvoicePrice"
}?.result
//      If theres no header value => calculate total invoice price
if (!totalInvoicePrice) {
    quote.lineItems.findAll {
        !it.folder
    }.each { lineItem ->
        BigDecimal lineItemTotalInvoicePrice = lineItem.outputs.find {
            it.resultName == "TotalInvoicePrice"
        }?.result

        if (!lineItemTotalInvoicePrice) {
            api.throwException("Unable to build workflow. Unable to calculate \
Total Invoice Price for entire quote. Missing for row ${lineItem.sku}")
        }

        totalInvoicePrice += lineItemTotalInvoicePrice
    }
}

def approvalLevels = api.findLookupTableValues("ApprovalLevelsRevenue")

approvalLevels.sort {
    it.name
}.each { level ->
    def threshold = level.attribute1
    def userGroup = level.attribute2

    if (totalInvoicePrice >= threshold) {
        workflow.addApprovalStep(userGroup)
                .withUserGroupApprovers(userGroup)
                .withReasons("Total Invoice Price is >= ${threshold} EUR")
    }
}



