def customerId = out.CustomerId as String
if (customerId == null) {
    api.throwException('Cannot retrieve Customer Region: Missing Customer id')
    return null
}
return api.customer('Region', customerId)
