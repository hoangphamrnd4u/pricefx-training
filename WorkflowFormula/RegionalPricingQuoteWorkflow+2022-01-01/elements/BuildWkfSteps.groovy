def totalInvoicePrice = out.TotalInvoicePrice
def region = out.Region as String
def userGroup
def threshold
if (region == null || totalInvoicePrice == null) {
    api.throwException('Cannot build Workflow: Missing parameter(s)')
}
def filters = [
        Filter.equal('key2', region)
]
def approvalThresholds = api.findLookupTableValues('ApprovalThreshold', *filters)
        .sort {
            it.key1
        }
        .each { thresholdLevel ->
            userGroup = thresholdLevel.attribute1
            threshold = thresholdLevel.attribute2

            if (totalInvoicePrice > threshold) {
                workflow.addApprovalStep(userGroup)
                        .withUserGroupApprovers(userGroup)
                        .withReasons("${userGroup}'s Approval: Total Invoice Price is > ${threshold}")
            }
        }
return approvalThresholds

