//  Find total invoice price of the quote
//      Check if there's total invoice price in quote header
def totalInvoicePrice = quote.outputs.find {
    it.resultName == "TotalInvoicePrice"
}?.result
//      If theres no header value => calculate total invoice price
if (!totalInvoicePrice) {
    totalInvoicePrice = 0.0
    quote.lineItems.findAll {
        !it.folder
    }.each { lineItem ->
        BigDecimal lineItemTotalInvoicePrice = lineItem.outputs.find {
            it.resultName == "TotalInvoicePrice"
        }?.result

        if (!lineItemTotalInvoicePrice) {
            api.throwException("Unable to build workflow. Unable to calculate \
Total Invoice Price for entire quote. Missing for row ${lineItem.sku}")
        }

        totalInvoicePrice += lineItemTotalInvoicePrice
    }
}
return totalInvoicePrice